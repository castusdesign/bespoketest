<?php

namespace App\Http\Controllers;

use App\enquiry;
use App\mail\EnquiryThanks;
use Illuminate\Http\Request;

/**
 * Class EnquiryController
 * @package App\Http\Controllers
 */
class EnquiryController extends Controller
{

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        $newEnquiry = enquiry::create(
            request()->validate([
                'name'=>'required',
                'phone'=>'',
                'email','email',
                'body'=>'required'
            ])
        );

        \Mail::to($newEnquiry->email)->send(
            new EnquiryThanks($newEnquiry)
        );

        return redirect('/')->with('message', 'Thank you for your enquiry.');
    }

}
