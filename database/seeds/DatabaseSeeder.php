<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
     /*   DB::table('services')->insert([
            'title' => 'Websites that tell a Story',
            'stub' => 'websites-that-tell-a-story',
            'snippet' => 'You have a story to tell that inspires you and you\'re really excited about telling it online. You need a partner who\'s going to share your excitement, get involved, be creative, and help you tell your story in an interesting way. We\'ve worked with musicians, non-profits, and brands that fit this description.'
        ]);
        DB::table('services')->insert([
            'title' => 'Advanced Marketing Websites',
            'stub' => 'advanced-marketing-websites',
            'snippet' => 'You\'re a B2B product or service company, you may have a website already but we\'re willing to bet it doesn\'t show you in your best light, and it certainly doesn\'t function as a useful marketing tool. We can help.'
        ]);
        DB::table('services')->insert([
            'title' => 'Great Value Brochure Websites',
            'stub' => 'great-value-brochure-websites',
            'snippet' => 'You need a website because not having one (or having a bad one) is holding you back – it\'s a barrier, preventing you from moving forward. You need a partner who\'s reliable, able to deliver on commitments, turn projects around on time and on budget, and deliver something that ticks all the boxes.'
        ]);
        DB::table('services')->insert([
            'title' => 'User Experience Led Websites',
            'stub' => 'user-experience-led-websites',
            'snippet' => 'You know your customers inside and out, and your website is central to your business. You don’t need a supplier, you need a partner who’s knowledgeable and willing to get to know your website as well as you do, and to help give your users the best possible user experience. Ecommerce sites fall into this category.'
        ]);
        DB::table('services')->insert([
            'title' => 'Complex Systems',
            'stub' => 'complex-systems',
            'snippet' => 'You need your website to integrate fully with, or operate as, your internal system. That could mean making bookings, creating invoices, managing teams, resources, orders and more.'
        ]);*/
    }
}
