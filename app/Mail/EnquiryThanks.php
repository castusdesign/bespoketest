<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnquiryThanks extends Mailable
{
    use Queueable, SerializesModels;

    public $newEnquiry;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($newEnquiry)
    {
        $this->newEnquiry = $newEnquiry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.enquiry-thanks');
    }
}
