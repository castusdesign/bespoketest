# Castus Bespoke Test
This is a simplified version of the Castus contact form, running on Laravel 5.7. 

You will require NodeJS (npm) and Composer to get this code up and running.

Run the following in the project root  
`composer install`  
`npm install`

Create a copy of .env.example called .env, and set up the database, you don't need to use MySQL, sqlite can be used, the
database path needs to be the absolute path.

`DB_CONNECTION=sqlite`  
`DB_HOST=127.0.0.1`  
`DB_PORT=3306`  
`DB_DATABASE=C:\Projects\castus-eval\database\database.sqlite`  
`DB_USERNAME=homestead`  
`DB_PASSWORD=secret`

You may also need mail settings, you can set up anything you like, but if you want to just fling your emails off into the void, feel free to use this mailtrap account:

`MAIL_DRIVER=smtp`  
`MAIL_HOST=smtp.mailtrap.io`  
`MAIL_PORT=2525`  
`MAIL_USERNAME=bb7a6ab4638e26`  
`MAIL_PASSWORD=9ed4b0cda2a4eb`  
`MAIL_ENCRYPTION=null`

Generate an application key by running  
`php artisan key:generate`

Set up the database  
`php artisan migrate:install`

Start serving the application  
`php artisan serve`  
This will provide you with a URL to access the application
    
Sass can be compiled with  
`npm run development`  
or you can set it up to watch for changes and automatically compile  
`npm run watch`  