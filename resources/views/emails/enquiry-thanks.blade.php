@component('mail::message')
# Hey!

Thanks for that email!
@if($newEnquiry->body)

Here's a copy of your message, for your records:

{{$newEnquiry->body}}

@endif

@component('mail::button', ['url' => 'https://www.castus.co.uk'])
Visit us again
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
